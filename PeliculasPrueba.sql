-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 07-07-2017 a las 06:57:16
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `PeliculasPrueba`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id_cliente` int(10) NOT NULL,
  `nombre` varchar(20) DEFAULT NULL,
  `usuario` varchar(20) NOT NULL,
  `contrasena` varchar(20) NOT NULL,
  `apellido` varchar(20) DEFAULT NULL,
  `corroe` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `nombre`, `usuario`, `contrasena`, `apellido`, `corroe`) VALUES
(1, 'damodar', 'damodar', 'damodar', 'navarrete', 'damodar@hotmai.com'),
(3, 'vanessa', 'vanessa', 'vanessa', 'ortega', 'vane@pecas.com'),
(9, 'vanesita', 'vanesita', 'vanesita', 'navarrete', 'vanesita@gmail.com'),
(14, 'diego', 'diego', 'diego', 'carilao', 'diego@gmail.com'),
(15, 'juan', 'juan', 'juan', 'emfermo', 'imbecil'),
(17, 'ringo', 'ringo', 'ringo', 'gonzales', 'ringo@hotmail.com'),
(18, 'manuel', 'manuel', 'manuel', 'morales', 'manuel@hotmail.com'),
(21, 'dino', 'dino', 'dino', 'dino', 'dino'),
(22, 'ariel', 'ariel', 'ariel', 'ariel', 'ariel');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pelicula`
--

CREATE TABLE `pelicula` (
  `id_pelicula` int(20) NOT NULL,
  `nombre_pelicula` varchar(20) DEFAULT NULL,
  `valor_pelicuala` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pelicula`
--

INSERT INTO `pelicula` (`id_pelicula`, `nombre_pelicula`, `valor_pelicuala`) VALUES
(1, 'thor', 10000),
(2, 'logan', 10000),
(3, 'Pawer Ranger', 10000),
(4, 'Liga Justicia', 10000),
(5, 'star wars', 10000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tranx`
--

CREATE TABLE `tranx` (
  `id_tranx` int(10) NOT NULL,
  `id_clinte_tranx` int(10) NOT NULL,
  `id_pelicula_tranx` int(10) NOT NULL,
  `tipo_pago` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tranx`
--

INSERT INTO `tranx` (`id_tranx`, `id_clinte_tranx`, `id_pelicula_tranx`, `tipo_pago`) VALUES
(36, 1, 1, 'tarjeta'),
(37, 1, 2, 'tarjeta'),
(38, 1, 1, 'tarjeta'),
(39, 1, 5, 'tarjeta'),
(40, 18, 1, 'tarjeta'),
(41, 14, 1, 'tarjeta'),
(42, 22, 1, 'tarjeta');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Indices de la tabla `pelicula`
--
ALTER TABLE `pelicula`
  ADD PRIMARY KEY (`id_pelicula`);

--
-- Indices de la tabla `tranx`
--
ALTER TABLE `tranx`
  ADD PRIMARY KEY (`id_tranx`),
  ADD KEY `foreign key` (`id_clinte_tranx`) USING BTREE,
  ADD KEY `tranx_ibfk_1` (`id_pelicula_tranx`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id_cliente` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT de la tabla `tranx`
--
ALTER TABLE `tranx`
  MODIFY `id_tranx` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tranx`
--
ALTER TABLE `tranx`
  ADD CONSTRAINT `tranx_ibfk_1` FOREIGN KEY (`id_pelicula_tranx`) REFERENCES `pelicula` (`id_pelicula`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
